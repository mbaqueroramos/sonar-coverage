# SonarCoverage

Extension to query Sonarqube and show a table with the coverage of each project listed by the query.
The table is intended to be heatmap of coverages. 

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Develop the extension

Run `npm run watch` to build the project and watch every changing file. The build artifacts will be stored in the `dist/` directory,
install the project as a chrome extension and test the changes in real time.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.4.

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## TODO
Filtrado por fecha ANTES de la carga
Revisar la version prev
Añadir tipo de proyecto ts xpl
