export interface ICompData {
  name?: string;
  comp?: string;
  owner: string;
  project: string;
  previousVersion?: string;
  previousCoverage?: number;
  latestVersion?: string;
  latestCoverage?: number;
  coverageDifference?: number;
  analysisDate?: Date;
  coverage?: string;
  qualityGate?: string;
  ncloc?: number,
  comment?: string;
  loading?: boolean;
}

export interface ComponentNavigation {
  key: string;
  organization: string;
  id: string;
  name: string;
  isFavorite: boolean;
  visibility: string;
  extensions: any[];
  version: string;
  analysisDate: string;
  qualityProfiles: QualityProfile[];
  qualityGate: QualityGate;
  breadcrumbs: Breadcrumb[];
}

export interface Breadcrumb {
  key: string;
  name: string;
  qualifier: string;
}

export interface QualityGate {
  key: number;
  name: string;
  isDefault: boolean;
}

export interface QualityProfile {
  key: string;
  name: string;
  language: string;
}
