export interface Paging {
  pageIndex: number;
  pageSize: number;
  total: number;
}

export interface Event {
  key: string;
  category: string;
  name: string;
}

export interface Analysis {
  key: string;
  date: Date;
  events: Event[];
}

export interface IAnalyses {
  paging: Paging;
  analyses: Analysis[];
}

