export interface IMeasures {
  paging: Paging;
  measures: Measure[];
  project?: string;
}

export interface Paging {
  pageIndex: number;
  pageSize: number;
  total: number;
}

export interface History {
  date: Date;
  value: string;
}

export interface Measure {
  metric: string;
  value?: string;
  component?: string;
  history?: History[];
  periods?: Period[];
}

interface Period {
  index: number;
  value: string;
}
