import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { CovDataService } from './cov-data.service';

describe('CovDataService', () => {
  let service: CovDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(CovDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
