import { Injectable } from '@angular/core';
import { ICompData } from '../model/comp';
import { Observable } from 'rxjs';

const SONAR_URL_EXTENSION = 'sonarUrl';
const JSON_PROJECTS_EXTENSION = 'jsonProjects';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService {



  private url: Observable<string>;
  private components: Observable<ICompData[]>;

  constructor() {
    this.url = new Observable(subscriber => {
      chrome.storage.local.get([SONAR_URL_EXTENSION], (result: any) => {
        subscriber.next(result.sonarUrl);
      });
    });

    this.components = new Observable(subscriber => {
      this.getJsonProjectsFromStorage(JSON_PROJECTS_EXTENSION).subscribe(jsonProjects => {
        console.log(JSON_PROJECTS_EXTENSION, jsonProjects);
        subscriber.next(JSON.parse(jsonProjects));
      });
    });
  }

  getUrl(): Observable<string> {
    return this.url;
  }

  getComponents(): Observable<ICompData[]> {
    return this.components;
  }

  setComponents(updatedComponents:ICompData[]):void {
    this.saveJsonProjects(JSON_PROJECTS_EXTENSION, JSON.stringify(updatedComponents));
  }

  getJsonProjectsFromStorage(inputId: string, aggregator = '', id = 0) {

    return new Observable<string>(subscriber => {
      const storageId = inputId + id;
      chrome.storage.local.get([storageId], (result: any) => {
        const value = result && result[storageId];
        if (value) {
          if (value.includes(']')) {
            subscriber.next(aggregator + value);
            subscriber.complete();
          } else {
            this.getJsonProjectsFromStorage(inputId, aggregator + value, id + 1).subscribe(aggregatedValue => {
              subscriber.next(aggregatedValue);
              subscriber.complete();
            });
          }
        }
      });
    });
  }

  saveFilters(filterName: string, filterOwner: string, filterDateStart: any, filterDateEnd: any) {
    const objToSave:any = {};
    objToSave['filterName'] = filterName;
    objToSave['filterOwner'] = filterOwner;
    objToSave['filterDateStart'] = filterDateStart;
    objToSave['filterDateEnd'] = filterDateEnd;
    console.log("saveFilters " + JSON.stringify(objToSave));
    chrome.storage.local.set(objToSave, () => {});
  }

  saveOrders(tableOrders: { field: string, order: number }[]) {
    const objToSave:any = {};
    objToSave['tableOrders'] = tableOrders;

    chrome.storage.local.set(objToSave, () => {});
  }

  getOrdersFromStorage(): Promise<any> {
    return new Promise(resolve => {
      chrome.storage.local.get(['tableOrders'], (result: any) => {
        resolve(result);
      });
    });
  }

  getFiltersFromStorage(): Promise<any> {
    return new Promise(resolve => {
      chrome.storage.local.get(['filterName', 'filterOwner', 'filterDateStart', 'filterDateEnd'], (result: any) => {
        resolve(result);
      });
    });
  }

  saveJsonProjects(inputId: string, jsonProjectsArray: string = '', id = 0, next = () => {}) {
    const objToSave:any = {};
    const maxBytes = chrome.storage.local.QUOTA_BYTES;
    if (jsonProjectsArray.length > maxBytes) {
      this.saveJsonProjects(inputId, jsonProjectsArray.slice(0, maxBytes), id, () => {
        setTimeout(() => { this.saveJsonProjects(inputId, jsonProjectsArray.slice(maxBytes), id + 1); }, 5);
      });
      return;
    }
    try {
      objToSave[inputId + id] = jsonProjectsArray;
      chrome.storage.local.set(objToSave, () => {});
      next();
    } catch (_syncError) {
      setTimeout(() => { this.saveJsonProjects(inputId, jsonProjectsArray, id, next); }, 5);
    }

  }
}
