import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { combineLatestWith, Observable, of } from 'rxjs';
import { ICompData, ComponentNavigation } from '../model/comp';
import { IMeasures, Measure } from '../model/measures';
import { IAnalyses, Analysis } from '../model/analyses';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root',
})
export class CovDataService {

  private maxComponentsPerPage = 10;

  constructor(private http: HttpClient, private configService: ConfigurationService) {

  }

  yyyyMMdd(currentDate:Date) {
    let mm:string|number = currentDate.getMonth() + 1;
    let dd:string|number = currentDate.getDate();
    let yyyy:string|number = currentDate.getFullYear();
    mm = (mm > 9 ? "" : "0") + mm;
    dd = (dd > 9 ? "" : "0") + dd;
    return yyyy + "-" + mm + "-" + dd;
  }


  private fetchSonarDataForComponent<T>(sonarApiQuery:string): Observable<T> {
    return new Observable(subscriber => {
      this.configService.getUrl().subscribe(sonarUrl => {
        this.http.get<T>(
          sonarUrl + sonarApiQuery
        ).subscribe(subscriber);
      });
    });
  }

  fetchCoverageForProjects(components: ICompData[]):Observable<ICompData[]> {
    return new Observable(subscriber => {
      if (components.length > this.maxComponentsPerPage) {
        this.fetchCoverageForProjects(components.slice(0, this.maxComponentsPerPage))
        .subscribe({
          next: (componentsResolved) => {
            subscriber.next(componentsResolved);
          },
          complete: () => {
            this.fetchCoverageForProjects(components.slice(this.maxComponentsPerPage)).subscribe({
              next: (componentsResolved) => {
              subscriber.next(componentsResolved);
            }, complete: () => subscriber.complete()
          });
        }});
      } else {
        components.forEach(component => { component.loading = true;});
        subscriber.next(components);
        if (components?.length) {
          this.fetchBaseCoverageForProjects(components).subscribe(measures => {
            const subpromises: Promise<ICompData>[] = [];
            components.forEach(component => {
              subpromises.push(new Promise(resolveComponent => {
                  this.refreshComponentCoverage(component, measures.measures).subscribe(componentSubscribed => {
                    component.loading = false;
                    resolveComponent(componentSubscribed);
                    subscriber.next(components);
                  })
              }));
            });
            Promise.all(subpromises).then(componentsResolved => {
              subscriber.complete();
            });
          });
        } else {
          subscriber.complete();
        }

      }
    });
  }

  refreshComponentCoverage(component: ICompData, measures: Measure[]):Observable<ICompData> {
    const coverageMeasure = this.findMeasureForComponent(component, measures, 'coverage');
    const nclocMeasure = this.findMeasureForComponent(component, measures, 'ncloc');
    const currentCoverage = coverageMeasure?.value;
    if (currentCoverage !== component.coverage) {
      component.coverage = currentCoverage;
      component.ncloc = Number(nclocMeasure?.value);
      return this.updateCoverageDataForComponent(component);
    }
    return of(component);
  }

  fetchBaseCoverageForProjects(components: ICompData[]) {
    let projects = components.map(component => component.project);
    return this.fetchSonarDataForComponent<IMeasures>(`/api/measures/search?metricKeys=coverage,ncloc&projectKeys=${projects}`);
  }

  fetchComponentNavigation(
    component: ICompData
  ): Observable<ComponentNavigation> {
    return this.fetchSonarDataForComponent<ComponentNavigation>(`/api/navigation/component?component=${component.project}`);
  }

  fetchMeasuresForComponent(
    component: ICompData,
    fromDate: string
  ): Observable<IMeasures> {
    return this.fetchSonarDataForComponent<IMeasures>(`/api/measures/search_history?component=${component.project}&metrics=coverage,ncloc&ps=500&from=${fromDate}`);
  }

  fetchAnalysesForComponent(
    component: ICompData,
    fromDate: string
  ): Observable<IAnalyses> {
    return this.fetchSonarDataForComponent<IAnalyses>(`/api/project_analyses/search?project=${component.project}&p=1&ps=500&from=${fromDate}&internal=true`);
  }

  updateCoverageDataForComponent(component: ICompData, fromDate = new Date()): Observable<ICompData> {

    fromDate.setMonth(fromDate.getMonth() - 3);
    const fromDateStr = this.yyyyMMdd(fromDate);
    // TODO
    const measures = this.fetchMeasuresForComponent(component, fromDateStr);
    const analyses = this.fetchAnalysesForComponent(component, fromDateStr);
    const componentNavigation = this.fetchComponentNavigation(component);
    return new Observable(subscriber => {
        measures.pipe(
        combineLatestWith(analyses, componentNavigation)
      )
      .subscribe({
        next: ([measures, analyses, componentNavigation]) => {
          if (measures.paging.total === 0 && component.coverage !== '0') {
            fromDate.setMonth(fromDate.getMonth() - 6);
            this.updateCoverageDataForComponent(component, fromDate).subscribe(recursiveSubscriber => {
              subscriber.next(recursiveSubscriber);
            });
          } else {
            const latestVersion = this.getLatestVersion(analyses);
            const latestVersionNumer = this.getVersionNumber(latestVersion);
            const previousVersion = this.getLatestVersion(analyses, latestVersionNumer);
            const previousVersionNumber = this.getVersionNumber(previousVersion);
            const latestCoverage = latestVersion ? this.getCoverageForDate(measures, latestVersion.date) : '';
            const previousCoverage = previousVersion ? this.getCoverageForDate(measures, previousVersion.date) : '';
            const ncloc = latestVersion ? this.getNumLinesOfCode(measures, latestVersion.date) : '';
            const qualityGate = this.getQualityGate(componentNavigation);
            component.analysisDate = latestVersion?.date;
            component.latestVersion = latestVersionNumer;
            component.latestCoverage = Number(latestCoverage) || Number(component.coverage);
            component.previousVersion = previousVersionNumber || latestVersionNumer;
            component.previousCoverage = Number(previousCoverage) || Number(component.coverage);
            component.ncloc = Number(ncloc);
            component.qualityGate = qualityGate;
            component.coverageDifference = component.latestCoverage - component.previousCoverage;
            subscriber.next(component);
          }
        }, error: () => {
          component.analysisDate = undefined;
          component.latestVersion = '-';
          component.latestCoverage = Number(component.coverage);
          component.previousVersion = '-';
          component.previousCoverage = Number(component.coverage);
          subscriber.next(component);
        }
      });
    });
  }


  /* obtiene la ultima version que sea distinta de otherVersion */
  getLatestVersion(analyses: IAnalyses, otherVersion:string = ''):Analysis|undefined {
    let firstDistinct: Analysis|undefined;
    for (let analysis of analyses.analyses) {
      let version = this.getVersionNumber(analysis);
      if (version && version !== otherVersion) {
        firstDistinct = firstDistinct || analysis;
        if (!otherVersion || (!version.includes('-RC') && !version.includes('-SNAPSHOT')) ) {
          return analysis;
        }
      }
    }
    return firstDistinct;
  }
  getVersionNumber(analysis: Analysis|undefined):string {
    if (analysis?.events?.length) {
      for (const event of analysis.events) {
        if (event.category === 'VERSION') {
          return event.name;
        }
      }
    }
    return '';
  }

  getQualityGate(componentNavigation: ComponentNavigation):string {
    try {
      const name = componentNavigation.qualityGate.name;
      return name.replace('-qualitygate', '');
    } catch (e) {
      console.log('error reading parameter componentNavigation.qualityGate.name', componentNavigation);
    }
    return '';
  }

  /* obtiene la cobertura que se produjo en una fecha de analisis */
  getCoverageForDate(measures: IMeasures, analysisDate:Date):string {
    for (const measure of measures.measures) {
      if (measure.metric === 'coverage' && measure.history) {
        for (let coverage of measure.history) {
          if (coverage.date === analysisDate) {
            return coverage.value;
          }
        }
      }
    }
    return '';
  }

  getNumLinesOfCode(measures: IMeasures, analysisDate:Date):string {
    for (const measure of measures.measures) {
      if (measure.metric === 'ncloc' && measure.history) {
        for (let coverage of measure.history) {
          if (coverage.date === analysisDate) {
            return coverage.value;
          }
        }
      }
    }
    return '';
  }

  getPreviousCoverage(element: any) {
    let previousCoverageIndex = element[0].history.length - 2;
    if (element[0].metric === 'coverage') {
      for (let i = previousCoverageIndex; i >= 0; i--) {
        for (let j = 0; j < element.analyses.length - 2; j++) {
          if (
            element.analyses[j].date === element[0].history[i].date &&
            !element.analyses[j].events[0].name.includes('-')
          ) {
            return element[0].history[i].value;
          }
        }
      }
    }
  }

  findMeasureForComponent(component: ICompData, measures: Measure[], metric: string):Measure|undefined {
    const project = component.project;
    return measures.find(measure => measure.component === project && measure.metric === metric);
  }
}
