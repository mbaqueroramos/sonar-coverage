import {Component, Inject, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ICompData } from 'src/app/model/comp';

@Component({
  selector: 'edit-row-dialog',
  templateUrl: './edit-row-dialog.component.html',
  styleUrls: ['./edit-row-dialog.component.css']
})
export class EditRowDialogComponent {
  filteredOwners: string[] = [];
  componente:any ={};
  onAdd= new EventEmitter();

  constructor(@Inject(MAT_DIALOG_DATA) public data: { component: ICompData, owners: string[]}) {
    if (!data.component) {
      data.component = { owner: '', project: '' };
    }
   }


  filterOwners() {
    this.filteredOwners = this.data.owners.filter(owner => owner && owner.toLowerCase().includes(this.data.component.owner.toLowerCase()) );
  }
  saveAndAddNewComp(){
    this.componente = this.data.component;
    this.onAdd.emit(this.componente);
    this.data.component = { owner: '', project: '' };
  }

  setprojectInName(component: ICompData) {
    component.comp = component.project.replace(/.*:/,'');
  }
}
