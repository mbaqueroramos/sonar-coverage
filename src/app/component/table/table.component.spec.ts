import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfigurationService } from '../../service/configuration.service';
import { CovDataService } from '../../service/cov-data.service';
import { HttpClientModule } from '@angular/common/http';
import { TableComponent } from './table.component';

import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatTableModule,
        MatDialogModule,
        FormsModule,
        MatIconModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatAutocompleteModule,
        MatProgressSpinnerModule],
      providers: [CovDataService,ConfigurationService],
      declarations: [ TableComponent ]
    })
    .compileComponents();

    window.chrome = window.chrome || {};
    const chromeMock = window.chrome;
    chromeMock.storage = chromeMock.storage || {};
    chromeMock.storage.local = chromeMock.storage.local || {};
    const chromeLocal = chromeMock.storage.local;
    chromeLocal.set = chromeLocal.set || function (_str, cb) { cb && cb()};
    chromeLocal.get = chromeLocal.get || function (_str, cb) { cb && cb({})};
    chromeLocal.QUOTA_BYTES = chromeLocal.QUOTA_BYTES || 10000000;

    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('title should be defined', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.mat-card-title')?.textContent).toContain('Coverage data');
  });

  it('config should contain data', (done) => {
    const mockJsonComponents = require('../../../../mocks/comp-data.json');
    spyOn((component as any).configService, "getComponents").and.returnValue(of(mockJsonComponents));
    spyOn((component as any).covDataService, "fetchCoverageForProjects").and.returnValue(of(mockJsonComponents));

    component.loadComponentsFromStorage().then(() => {
      component.initTableComponentsAndFilters().then(() => {
        expect(component.components).toBeDefined();
        expect(component.components.length).toEqual(1003);

        expect(component.owners).toBeDefined();
        expect(component.owners.length).toEqual(29);
        done();
      })
    });
  });

  it('config should not contain data', (done) => {

    spyOn((component as any).configService, "getComponents").and.returnValue(of([]));
    spyOn((component as any).covDataService, "fetchCoverageForProjects").and.returnValue(of([]));

    component.loadComponentsFromStorage().then(() => {
      component.initTableComponentsAndFilters().then(() => {
        expect(component.components).toBeDefined();
        expect(component.components.length).toBe(0);

        expect(component.owners).toBeDefined();
        expect(component.owners.length).toBe(0);
        done();
      })
    });
  });
});
