import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CovDataService } from '../../service/cov-data.service';
import { ConfigurationService } from '../../service/configuration.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MatTable } from '@angular/material/table';
import { ICompData } from '../../model/comp';
import { EditRowDialogComponent } from '../edit-row/edit-row-dialog.component';
import { EditJsonDialogComponent } from '../edit-json-dialog/edit-json-dialog.component';
import { Clipboard } from "@angular/cdk/clipboard";
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  @ViewChild('componentsTable')
  componentsTable: MatTable<any> | null = null;

  //arrays donde almaceno los datos del archivo JSON
  components: ICompData[] = [];
  compData: ICompData[] = [];

  extData: any = [];
  commentList: any = [];
  sonarUrl: string = '';

  editMode: boolean = false;

  //nombre de las columnas de la tabla
  displayedColumns: string[] = [
    'name',
    'source',
    'owner',
    'qualityGate',
    'previousVersion',
    'previousCoverage',
    'latestVersion',
    'latestCoverage',
    'coverageDifference',
    'analysisDate',
    'comment',
  ];

  owners: string[] = [];
  filteredOwners: string[] = [];
  qualityGates: string[] = [];
  filteredQualityGates: string[] = [];
  coverageDiffOrder: number = 0;
  incorporandoDatos: boolean = false;

  tableOrders: { field: string, order: number }[] = [];

  /* variables para los input por los que se filtra */
  name: string = '';
  owner: string = '';
  qualityGate: string = '';
  range = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });
  public loading: boolean | string = false;
  private lastLoadingChange: number = 0;

  constructor(
    private covDataService: CovDataService,
    private configService: ConfigurationService,
    private changeDetectorRefs: ChangeDetectorRef,
    private clipboard: Clipboard,
    public dialog: MatDialog
  ) {}

  ngOnInit() {

    this.loadAllItemsFromStorage().then(() => {
      this.applyFilters();
      this.initTableComponentsAndFilters().then(() => {
        this.applyAllOrders();
      }).catch(() => {
        this.loading = false;
      })
    });
  }

  loadAllItemsFromStorage() {
    const promises = [];
    promises.push(this.loadFiltersFromStorage());
    promises.push(this.loadOrderFromStorage());
    promises.push(this.loadComponentsFromStorage());
    promises.push(new Promise(resolve => {
      this.configService.getUrl().subscribe((url) => {
        this.sonarUrl = url;
        resolve(this.sonarUrl);
      });
    }));
    return Promise.allSettled(promises);
  }

  async ngDoCheck() {
    // Nothing to do
  }

  initTableComponentsAndFilters() {

    this.reloadOwners();
    this.reloadQualityGates();
    return this.loadAndUpdateAllComponents();
  }

  reloadOwners() {
    this.owners = this.components
      .map((component) => component.owner)
      .filter((item, index, arr) => {
        return arr.indexOf(item) === index;
      })
      .sort();
    this.filterOwners();
  }

  filterOwners() {
    this.filteredOwners = this.owners.filter(
      (owner) => owner && owner.toLowerCase().includes(this.owner.toLowerCase())
    );
  }

  reloadQualityGates() {
    this.qualityGates = this.components
      .map((component) => (component.qualityGate || ''))
      .filter((item, index, arr) => {
        return arr.indexOf(item) === index;
      })
      .sort();
      console.log('@mbaquerr', this.qualityGates);
    this.filterQualityGates();
  }

  filterQualityGates() {
    this.filteredQualityGates = this.qualityGates.filter(
      (qualityGate) => qualityGate && qualityGate.toLowerCase().includes(this.qualityGate.toLowerCase())
    );
  }

  loadComponentsFromStorage() {
    return new Promise((resolve) => {
      this.configService.getComponents().subscribe((components) => {
        this.components = components.map(c => {
          if (!c.owner) {
            c.owner = '';
          }
          if (!c.comp) {
            c.comp = c.project.replace(/.*:/,'');
          }
          return c;
        });
        this.compData = this.components;
        resolve(this.compData);
      });
    });
  }

  getGitLink(sonarUrl: string, element: ICompData) {
    try {
      const gitUrl = sonarUrl.replace('softwarequality', 'sourcerepository');
      const project = element.project.split(':');
      const gitProjectName = project.pop();
      const gitProjectType = project.pop()?.split('.');
      const gitProjectType1 = gitProjectType?.pop();
      const gitProjectType2 = gitProjectType?.pop();
      const gitproject = gitProjectName + '-' + gitProjectType2 + '-' + gitProjectType1;
      return gitUrl + '/' + element.qualityGate + '/' + gitproject + '/activity';
    } catch (error) {
      return '#';
    }
  }

  loadComponentsFromStorageAndUpdateTable() {
    return this.loadComponentsFromStorage().then(() => {
      this.reloadOwners();
      this.refreshTable();
    });
  }

  refreshTable():void {
    if (this.componentsTable) {
      this.componentsTable.renderRows();
      this.changeDetectorRefs.detectChanges();
    }
  }

  createFormControlForStr(str:string):FormControl<Date | null> {
    const dateParsed = Date.parse(str);
    return new FormControl<Date | null>(dateParsed ? new Date(dateParsed) : null);
  }

  loadFiltersFromStorage() {
    return this.configService.getFiltersFromStorage().then((filters) => {
      if (filters) {
        this.name = filters.filterName || '';
        this.owner = filters.filterOwner || '';
        if (filters.filterDateStart || filters.filterDateEnd) {
          this.range = new FormGroup({
            start: this.createFormControlForStr(filters.filterDateStart),
            end: this.createFormControlForStr(filters.filterDateStart)
          });
        }
      }
    });
  }

  switchOrderInit(orderParameter: number) {
    if (orderParameter === 1) {
      return 0;
    }
    if (orderParameter === -1) {
      return 1;
    }
    return 0;
  }

  loadOrderFromStorage() {
    return this.configService.getOrdersFromStorage().then((orders) => {
      this.tableOrders = orders.tableOrders || [];
    });
  }

  applyAllOrders() {
    for (const order of this.tableOrders) {
      this.orderByOrder(order);
    }
    this.refreshTable();
  }

  /* reestablece los filtros y la tabla */
  reset() {
    this.cleanFilter();
    return this.loadComponentsFromStorage();
  }

  /* obtiene los datos de analisis y measures */
  loadAndUpdateAllComponents() {
    return this.reloadComponents(this.compData).then((_resolvedCompData) => {
      console.log('loadAndUpdateComponents Finished');
      this.saveComponents();
    });
  }

  reloadComponents(components: ICompData[]): Promise<ICompData[]> {
    this.loading = true;
    return new Promise<ICompData[]>((resolve) =>
      this.covDataService.fetchCoverageForProjects(components).subscribe({
        next: (_resolvedCompData) => {
          this.refreshTable();
          this.changeLoading();
        },
        complete: () => {
          resolve(components);
          this.loading = false;
          this.refreshTable();
        },
      })
    );
  }

  changeLoading () {

    const currLoadingChange = +new Date();
    if (currLoadingChange - this.lastLoadingChange > 500) {
      if (this.loading === 'Loading.') {
        this.loading = 'Loading..';
      } else if (this.loading === 'Loading..') {
        this.loading = 'Loading...';
      } else if (this.loading === 'Loading...') {
        this.loading = 'Loading....';
      } else {
        this.loading = 'Loading.';
      }
      this.lastLoadingChange = currLoadingChange;
    }
  }

  /* almaceno todos los componentes chrome storage */
  saveComponents() {
    this.configService.setComponents(this.components);
  }

  /* limpia los imputs */
  cleanFilter() {
    this.name = '';
    this.range.reset();
    this.owner = '';
  }

  /* aplica los filtros correspondientes al pulsar el boton de busqueda */
  applyFilters() {
    this.configService.saveFilters(
      this.name,
      this.owner,
      this.range.value.start ? this.range.value.start.toISOString() : '',
      this.range.value.end ? this.range.value.end.toISOString() : ''
    );
    this.compData = this.components;
    this.filterName();
    this.filterDate();
    this.filterOwner();
    this.filterQualityGates();
    this.refreshTable();
  }

  /* filtra la tabla por el nombre*/
  filterName() {
    this.compData = this.compData.filter((item: any) => {
      return item.comp
        .replace(/[^:]*:/, '')
        .toUpperCase()
        .includes(this.name.toUpperCase());
    });
    /* save in storage */
  }

  /* filtra la tabla por el owner*/
  filterOwner() {
    this.compData = this.compData.filter((item: any) => {
      if (item.owner !== undefined) {
        return item.owner.toUpperCase().includes(this.owner.toUpperCase());
      }
    });
  }

  filterQualityGate() {
    this.compData = this.compData.filter((item: any) => {
      if (item.qualityGate !== undefined) {
        return item.owner.toUpperCase().includes(this.qualityGate.toUpperCase());
      }
    });
  }

  /* filtra la tabla por la fecha*/
  filterDate() {
    const start = this.range.value.start;
    const end = this.range.value.end;
    if (start && end) {
      this.compData = this.compData.filter((item: any) => {
        const date = new Date(item.analysisDate);
        return date >= start && date <= end;
      });
    } else if (start) {
      this.compData = this.compData.filter((item: any) => {
        const date = new Date(item.analysisDate);
        return date >= start;
      });
    } else if (end) {
      this.compData = this.compData.filter((item: any) => {
        const date = new Date(item.analysisDate);
        return date <= end;
      });
    }
  }

  /* establece un color determinado a la fila de la tabla en relacion al valor de coverage */
  setColumnColor(coverage: number) {
    if (coverage === null || coverage === undefined) {
      return 'rgb(255,255,255)';
    } else if (coverage < 50) {
      return 'rgb(255,0,0)';
    } else if (coverage < 65) {
      return 'rgb(255,100,0)';
    } else if (coverage < 70) {
      return 'rgb(255,255,0)';
    } else if (coverage < 80) {
      return 'rgb(190,245,0)';
    } else {
      return 'rgb(0,255,0)';
    }
  }

  setDifferenceColor(coverageDifference: number) {
    if (coverageDifference < 0) {
      return 'rgb(255,0,0)';
    } else if (coverageDifference >= 5) {
      return 'rgb(140,140,255)';
    } else if (coverageDifference >= 2) {
      return 'rgb(0,255,0)';
    } else if (coverageDifference >= 1) {
      return 'rgb(180, 255, 180)';
    } else if (coverageDifference > 0) {
      return 'rgb(210, 255, 210)';
    } else {
      return 'rgb(255,255,255)';
    }
  }

  switchOrder(orderParameter: number) {
    if (orderParameter === 0) {
      return 1;
    }
    if (orderParameter === 1) {
      return -1;
    }
    return 0;
  }


  stringCompareIgnoreCase(
    a: string | undefined,
    b: string | undefined,
    asc: number = 1
  ): number {
    const ai = (a || '').toUpperCase().trim();
    const bi = (b || '').toUpperCase().trim();
    return ai === bi ? 0 : ai > bi ? asc : -asc;
  }

  numberCompare(a: number | undefined, b: number | undefined, asc: number = 1) {
    const ai = a || 0;
    const bi = b || 0;
    return ai === bi ? 0 : ai > bi ? asc : -asc;
  }

  dateCompare(a: Date | undefined, b: Date | undefined, asc: number = 1) {
    const ai = a || new Date(0);
    const bi = b || new Date(0);
    return ai === bi ? 0 : ai > bi ? asc : -asc;
  }

  isOrder(field: string, value: number):boolean {
    return this.getOrder(field).order === value;
  }

  getOrder(field: string) {
    let order = this.tableOrders.find(order => order.field === field);
    if (!order) {
      order = { field, order: 0 };
    }
    return order;
  }

  removeOrder(field: string) {
    this.tableOrders = this.tableOrders.filter(order => order.field !== field);
  }

  addOrder(order: { field: string; order: number; }) {
    this.removeOrder(order.field);
    this.tableOrders.push(order);
  }

  orderBy(field: string, newOrder: number):void {
    const order = this.getOrder(field);
    order.order = newOrder;
    console.log("orderBy", order);
    if (this.orderByOrder(order)) {
      this.addOrder(order);
      console.log("ok: ", this.tableOrders);
    } else {
      this.removeOrder(order.field);
    }
    console.log("refreshTable");
    this.refreshTable();
    this.configService.saveOrders(this.tableOrders);
  }

  orderByOrder(order: { field: string; order: number; }):boolean {
    console.log("orderByOrder", order);
    if (order.order) {
      this.compData.sort((a: ICompData, b: ICompData) => {
        const aValue = (a as any)[order.field];
        const bValue = (b as any)[order.field];
        if (typeof aValue === 'string') {
          console.log("orderByOrder as string");
          return this.stringCompareIgnoreCase(aValue, bValue, order.order);
        } else if (typeof aValue === 'number') {
          console.log("orderByOrder as number");
          return this.numberCompare(aValue, bValue, order.order);
        }
        console.log("orderByOrder as date");
        return this.dateCompare(aValue, bValue, order.order);
      });
      return true;
    }
    return false;
  }

  openEditAllPopup() {
    console.log('openEditAllPopup');
    const dialogRef = this.dialog.open(EditJsonDialogComponent);

    dialogRef.afterClosed().subscribe((result) => {
      console.log('openEditAllPopup result: ', result);
      this.reset();
    });

    dialogRef.componentInstance.reloadComponents = () => this.reloadComponents(this.compData);
  }

  openAddComponentPopup() {
    console.log('openAddComponentPopup');

    const dialogRef = this.dialog.open(EditRowDialogComponent, {
      data: { owners: [...this.owners] },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.comp && result.project) {
        console.log('openAddComponentPopup result: ', result);
        this.addComponent(result);
      }
    });
    dialogRef.componentInstance.onAdd.subscribe((result: any) => {
      console.log('openAddComponentPopup result: ', result);
      if (result.comp && result.project) {
        this.addComponent(result);
      }
    });
  }

  addComponent(component: ICompData): void {
    this.components.push(component);
    this.initTableComponentsAndFilters();
  }

  copyToClipboard(): void {
    let textToCopy = '';
    for (const component of this.compData) {
      let analisysStr = '-';
      if (component.analysisDate) {
        analisysStr = formatDate(component.analysisDate, 'yyyyMMdd', 'en-US');
      }
      textToCopy += component.comp + '\t' + analisysStr + '\t' + (component.coverage ?? '-') + '\t' + (component.ncloc ?? '-') + '\n';

    }
    this.clipboard.copy(textToCopy);
  }


}
