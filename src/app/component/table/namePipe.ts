import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'NamePipe' })
export class NamePipe implements PipeTransform {
    /* borra todo lo que haya antes de los dos puntos */
    expReg = /[^:]*:/;
    transform(value: string): string {
        return value.replace(this.expReg, '');
    }
}