import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from '../../service/configuration.service';
import { ICompData } from 'src/app/model/comp';

@Component({
  selector: 'app-edit-json-dialog',
  templateUrl: './edit-json-dialog.component.html',
  styleUrls: ['./edit-json-dialog.component.css']
})
export class EditJsonDialogComponent implements OnInit {

  componentsTxt: string = '';

  public reloadComponents = () => {};

  constructor(private configService: ConfigurationService) { }

  ngOnInit(): void {

    this.configService.getComponents().subscribe(components => {

      this.componentsTxt = JSON.stringify(components.map(c => { return { 'owner': c.owner, 'project': c.project } }), null, 4);

    });

  }

  saveJsonProjects(inputId="jsonProjects",jsonProjectsArray = this.componentsTxt, id = 0) {

    this.configService.getComponents().subscribe(components => {
      const plainProjects = JSON.parse(jsonProjectsArray);
      const filledProjects = plainProjects.map((plainComponent:any) => {
        const project = plainComponent.project;
        if (components) {
          return { ...components.find(c => c.project === project), ...plainComponent };
        }
        return plainComponent;
      });
      return this.configService.saveJsonProjects(inputId, JSON.stringify(filledProjects), id, this.reloadComponents);
    });

  }

}
