import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { AppComponent } from './app.component';
import { CovDataService } from './service/cov-data.service';
import { ConfigurationService } from './service/configuration.service';
import { NamePipe } from './component/table/namePipe';
import { TableComponent } from './component/table/table.component';
import { EditRowDialogComponent } from './component/edit-row/edit-row-dialog.component';
import { EditJsonDialogComponent } from './component/edit-json-dialog/edit-json-dialog.component';
import { OrderTableComponent } from './component/order-table/order-table.component';
import { ClipboardModule } from "@angular/cdk/clipboard";


@NgModule({
  declarations: [AppComponent, TableComponent,NamePipe, EditRowDialogComponent, EditJsonDialogComponent, OrderTableComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ClipboardModule,
    HttpClientModule,
    MatCardModule,
    MatTableModule,
    MatDialogModule,
    FormsModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule
  ],
  providers: [CovDataService,ConfigurationService],
  bootstrap: [AppComponent],
})
export class AppModule {}
