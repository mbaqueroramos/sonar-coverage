"use strict";

(function () {
  document.addEventListener("DOMContentLoaded", initValuesFromStorage);

  function initValuesFromStorage() {
    setupInputs()
    setupListeners();
  }

  function setupInputs() {
    setInputValueFromStorage("sonarUrl");
  }

  function setupListeners() {
    document
      .getElementById("splashPageButton")
      .addEventListener("click", () => {
        window.open(chrome.runtime.getURL("index.html"), "_blank");
      });
    addListenerForInput("sonarUrl");
  }

  function addListenerForInput(inputId) {
    document.getElementById(inputId).addEventListener("keyup", (evt) => {
      const objToSave = {};
      objToSave[inputId] = evt.target.value;
      chrome.storage.local.set(objToSave, () => {});
    });
  }

  function setInputValueFromStorage(inputId) {
    chrome.storage.local.get([inputId], (result) => {
      if (result && result[inputId]) {
        document.getElementById(inputId).value = result[inputId];
      }
    });
  }

})();
